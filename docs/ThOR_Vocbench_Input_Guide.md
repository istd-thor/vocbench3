Summary

**[Introduction](#introduction) [1](#introduction)**

**[Data Input](#data-input) [1](#data-input)**

> [Lexicons](#lexicons) 2
>
> [Lexical Entry](#lexical-entry) [3](#lexical-entry)
>
> [Lexical Relations](#lexical-relations) [4](#lexical-relations)
>
> [Lexical Sense](#lexical-sense) [7](#lexical-sense)
>
> [From a Lexical Entry with an Associated Ontology
> Entity](#from-a-lexical-entry-with-an-associated-ontology-entity)
> [7](#from-a-lexical-entry-with-an-associated-ontology-entity)
>
> [New Lexical Sense without
> Associations](#new-lexical-sense-without-associations)
> [9](#new-lexical-sense-without-associations)
>
> [Sense Relations](#sense-relations) [13](#sense-relations)
>
> [Preferred Sense](#preferred-sense) [16](#preferred-sense)
>
> [Definition of Lexical Senses](#definition-of-lexical-senses)
> [17](#definition-of-lexical-senses)
>
> **[Ontology Entities](#ontology-entities) [20](#ontology-entities)**
>
> [Mapping Relations](#mapping-relations) [22](#mapping-relations)
>
> [Context](#context) [23](#context)
>
> **[Metadata](#metadata) [25](#metadata)**

**[Exporting Data](#exporting-data) [29](#exporting-data)**

# Introduction

This document is intended to present how to manually input data on
Vocbench considering the ThOR Project's specific requirements.

As a complement, this document's last section presents how the imported
data can be exported from Vocbench to be later used in other software
(e.g., the SKOS Generator created for the project).

The Vocbench installation (version 10.1) for the ThOR Project is
available at
[[https://thor-vocbench.unibz.it/vocbench3]{.underline}](https://thor-vocbench.unibz.it/vocbench3).

The Lexical Concepts, which are going to constitute the ThOR's
Thesaurus, are not included in this manual because they are
automatically generated from data here described.

# Data Input

This section describes how to manually input data on Vocbench. The data
must be inputted following the ThOR Data Model, which is going to be
presented in parts in this document. to accomplish the project's
requirements, the data model uses a subset of SKOS[^1], Ontolex[^2] and
Lexinfo[^3] concepts, as well as concepts created specifically for this
project.

The ThOR Data Model central concept is the Lexical Sense, which acts as
an intermediate concept between Lexical Entries, Lexical Concepts, and
Ontology Entities (implemented in Vocbench simply as an external URL
reference). The data model's main concepts can be visualized in the
figure below.

![](media/image1.png){width="6.267716535433071in"
height="3.4166666666666665in"}

After the mandatory creation of
[[Lexicons]{.underline}](#_rrcjcyv9xlcm), the data input for ThOR on
Vocbench begins with the input of Lexical Entries. The second step is
the creation of Lexical Senses and their related Ontology Entities, when
available. This document is going to present the creation of all these
concepts and others present at the data model.

## Lexicons

Once a project is created on Vocbench, the first entities to be created
are the Lexicons, which are mandatory in Vocbench for the input of
Lexical Entries. A Lexicon is described as [a collection of Lexical
Entries for a particular language]{.mark}[^4].

To create a Lexicon at Vocbench, follow the steps presented in the image
below.

![](media/image2.png){width="6.267716535433071in"
height="3.5694444444444446in"}

The Lexical Entries created on Vocbench are grouped on the selected
Lexicon.

## Lexical Entry

A Lexical Entry "*is a word, multiword expression or affix with a single
part-of-speech, morphological pattern, etymology and set of
senses*"[^5].

To create a new Lexical Entry at Vocbench, first select the Lexicon in
which it is going to be contained in and then follow the steps presented
in the image below.

![](media/image3.png){width="6.267716535433071in"
height="3.236111111111111in"}

According to the Ontolex specification, "*the canonical form property
relates a Lexical Entry to its canonical or dictionary form. This
usually indicates the \"lemma\" form of a Lexical Entry"* and it must be
unique[^6].

### Lexical Relations

Two Lexical Entries can be related to each other through lexical
relations. To insert a lexical relation between two Lexical Entries,
first select the desired source Lexical Entry, and inside its view click
to add a new "Other property".

![](media/image4.png){width="6.267716535433071in"
height="3.9583333333333335in"}

When the select property box opens, choose one of the
vartrans:lexicalRel available.

![](media/image5.png){width="5.526042213473316in"
height="5.838144138232721in"}

Note that the ThOR Data Model limits the options in lexinfo:fullFormFor,
lexinfo:contractionFor, and lexinfo:homograph, as can be seen in the
diagram below.

![](media/image6.png){width="4.777600612423447in"
height="3.255067804024497in"}

Finally, at the new pop-up box, select the desired destination Lexical
Entry.

![](media/image7.png){width="6.267716535433071in"
height="3.861111111111111in"}

The relation between the two Lexical Entries can then be seen in the
source entry's view.

![](media/image8.png){width="6.267716535433071in"
height="3.9722222222222223in"}

## Lexical Sense

A Lexical Sense "*represents the lexical meaning of a Lexical Entry when
interpreted as referring to the corresponding ontology element. A
Lexical Sense thus represents a reification of a pair of a uniquely
determined Lexical Entry and a uniquely determined Ontology Entity it
refers to. A link between a Lexical Entry and an Ontology Entity via a
Lexical Sense object implies that the Lexical Entry can be used to refer
to the Ontology Entity in question*"[^7].

On Vocbench, there are two ways to create Lexical Senses: [[the first
one]{.underline}](#from-a-lexical-entry-with-an-associated-ontology-entity)
is from an already created Lexical Entry, which requires the creation of
an associated Ontology Entity (denotes relation from Lexical Entry to an
Ontology Entity), and [[the second
one]{.underline}](#new-lexical-sense-without-associations) is to create
a new Lexical Sense without any relations and, after that, establish all
the necessary relations. Both ways are going to be explained in the
following sections.

### From a Lexical Entry with an Associated Ontology Entity

It is possible to directly create Lexical Sense in the Lexical Entry
view only when it is known the Ontology Entity to be associated with a
Lexical Entry. In this case, click on the blue rectangle ("Add Lexical
Sense") represented inside the red box in the image below.

![](media/image9.png){width="6.267716535433071in"
height="3.8333333333333335in"}

A pop-up box is going to ask for a reference, which is the Ontology
Entity to be related to the Lexical Entry. Add the desired Ontology
Entity URL and click on OK.

![](media/image10.png){width="6.267716535433071in" height="1.875in"}

As a result, it can be noted that a Lexical Sense was created and that
the Lexical Entry now has a denotes relation with the Ontology Entity
informed.

![](media/image11.png){width="6.267716535433071in"
height="3.736111111111111in"}

Accessing the created Lexical Sense (double click on its name), it can
be seen that it has the isSenseOf relation with the Lexical Entry and
the reference relation with the external URL of the Ontology Entity (lay
the mouse cursor on the globe to see the URL).

![](media/image12.png){width="6.267716535433071in"
height="3.3194444444444446in"}

If desired, the URL of the Lexical Sense can be renamed using the button
indicated in the image above.

### New Lexical Sense without Associations

to create a Lexical Sense without a relation with an Ontology Entity,
follow the steps indicated in the figure below.

![](media/image13.png){width="3.7115485564304462in" height="4.79375in"}

At the pop-up window, enter the URI of the new Lexical Sense to be
created and press OK.

![](media/image14.png){width="6.267716535433071in"
height="1.6527777777777777in"}

To relate this Lexical Sense with a Lexical Entry already created, first
double click the name of the Lexical Sense to open its property's view.

![](media/image15.png){width="3.615100612423447in"
height="2.1206364829396325in"}

Click on the blue rectangle to add a new property of the Lexical Sense.

![](media/image16.png){width="6.267716535433071in" height="3.625in"}

Select isSenseOf in the list of available relations and press OK.

![](media/image17.png){width="6.267716535433071in"
height="4.305555555555555in"}

In the new window, click on the Lexical Entry class on the left side and
select the desired Lexical Entry on the right side. Press OK.

![](media/image18.png){width="6.267716535433071in" height="3.875in"}

After this set of steps, it can be seen that the isSenseOf relation was
created from the Lexical Sense to the selected Lexical Entry.

![](media/image19.png){width="6.267716535433071in"
height="3.8055555555555554in"}

### Sense Relations

Relations between Lexical Senses can be configured using the same set of
steps described in the previous section. When clicking to add a new
property, it is possible to select one of the sense relations (senseRel)
made available by Vocbench.

![](media/image20.png){width="6.267716535433071in"
height="7.569444444444445in"}

After selecting the desired sense relation, press OK and a new window is
going to be shown. In this window, select the Lexical Sense class in the
left part, select the Lexical Entry which has the sense relation with
the desired Lexical Sense, and then select the desired Lexical Sense
(the available Lexical Senses are only going to be presented after the
selection of the related Lexical Entry). Press OK to conclude the
operation.

![](media/image21.png){width="6.267716535433071in"
height="5.041666666666667in"}

After the conclusion, it can be seen that the relationship was
established between the Lexical Senses.

![](media/image22.png){width="6.267716535433071in"
height="2.1944444444444446in"}

Note that the ThOR Data Model restricts the options of selectable sense
relations to Lexinfo properties synonym, relatedTerm, hypernym, as can
be seen in the following diagram. The hyponym relation is derived from
its inverse, the hypernym.

![](media/image23.png){width="5.171875546806649in"
height="3.496599956255468in"}

### Preferred Sense

Lexical Senses can be specialized in Preferred Senses. This subclass was
created to assist the SKOS Generator in correctly identifying which
skos:prefLabel should be set for the generated Lexical Concepts. The
SKOS prefLabels to be used are going to be the ones from the Lexical
Entries associated with the Preferred Senses.

It is important to mention that each SynSet (the set of all Lexical
Senses that are synonymous between themselves) must have exactly one
Preferred Sense.

For setting a Lexical Sense as a Preferred Sense, on the sense's view
(double click on the sense's name), click on the add button to add a new
type, just like represented in the figure below.

![](media/image24.png){width="6.267716535433071in"
height="2.0277777777777777in"}

A pop-up window is going to appear. In this window, select the class
thor:PreferredSense and click on ok.

![](media/image25.png){width="6.267716535433071in"
height="3.4444444444444446in"}

By the end of this property, it can be noted in the Lexical Sense view
that it has correctly received the Preferred Sense type.

### Definition of Lexical Senses

To be able to associate a definition to a specific source, it is
necessary to perform an action known in modeling as reification, which
consists of, instead of relating the characteristics to the element, it
is created an individual that aggregates these characteristics, and then
the association is done to this individual.

For creating a definition of a Lexical Sense, on the sense's view, first
click to add a new "other property", like indicated in the figure below.

![](media/image26.png){width="6.267716535433071in"
height="3.4305555555555554in"}

Mark the "show all" box and search for the term "definition". Select the
option skos:definition and click ok.

![](media/image27.png){width="6.267716535433071in"
height="5.166666666666667in"}

In the new pop-up, select the option **resource** and click OK.

In the add definition window, first select the class thor:Definition on
the class hierarchy at the left side of the box. After that, click on
the purple diamond indicated in the image below to create a new
individual.

![](media/image28.png){width="6.267716535433071in"
height="3.4444444444444446in"}

A pop-up is going to require you to insert the URI of the individual to
be created. Enter the URI and press ok. At the end of this process, the
new individual is going to appear in the sense definition, like shown in
the image below.

![](media/image29.png){width="6.267716535433071in"
height="4.083333333333333in"}

To insert the definition and any other information on the new
individual, double click on it to open its view.

To insert a textual definition to the individual, click on "add another
property" as presented below and enter an rdf:value.

![](media/image30.png){width="6.267716535433071in" height="2.375in"}

For inserting a source for the definition, click again to add another
property and create a dcterms:source relation.

## Ontology Entities

Ontology Entities can be created using the procedure already mentioned
in [[From a Lexical Entry with an Associated Ontology
Entity]{.underline}](#from-a-lexical-entry-with-an-associated-ontology-entity).
Besides that, new Ontology Entities can also be created from Lexical
Senses using the following steps.

First, click on the class tab (step 1) and then select the
ontolex:LexicalSense class (step 2). Select the Lexical Sense which is
going to be related to the Ontology Entity (step 3) and click on the
blue rectangle at the "Other properties" section to add a new property
(step 4).

![](media/image31.png){width="6.267716535433071in"
height="4.541666666666667in"}

Click on show all and select ontolex:reference. Click on OK.

![](media/image32.png){width="6.260416666666667in"
height="4.291666666666667in"}Select the option **resource** on the new
pop-up.

![](media/image33.png){width="6.267716535433071in" height="2.0in"}

On the left part of the new pop-up, select the class rdf:Resource, and
on the right part of the pop-up click on the purple diamond icon for the
creation of a new instance.

![](media/image34.png){width="6.267716535433071in"
height="3.513888888888889in"}

Unlock the edition of the URI by clicking on the locker icon and write
the desired URI. Click on OK.

![](media/image35.png){width="6.267716535433071in"
height="1.6527777777777777in"}

After that, the relation between the Lexical Sense and the Ontology
Entity is going to be shown in the Lexical Sense's view.

## Mapping Relations

Lexical Concepts, Lexical Senses, and Ontology Entities can be mapped
between themselves through five types of relationships, called mapping
properties in the data model. These properties are broaderMapping,
narrowerMapping, relatedMapping, exactMapping and closeMapping.

![](media/image36.png){width="5.244792213473316in"
height="3.7985531496062994in"}

To set one of these relationships, on a Semantic Element (Lexical
Concepts, Lexical Senses, or Ontology Entities) view in Vocbench, click
to add a new "other property", mark the option to show all properties
available, and select the desired one. In the same way already presented
for other cases, select the desired Semantic Element to be related and
press ok.

## Context

Lexical Senses are related to Contexts, a class defined in the ThOR
Ontology. These contexts can be classified in iStandards, Organizations,
or Communities. The Context class and its relations can be seen in the
figure below.

![](media/image37.png){width="3.4218755468066493in"
height="3.7748687664041993in"}

For creating an instance of the Context class, proceed as can be seen in
the figure below. On the Class tab, search for the "context" in the
search box. Finally select in the class taxonomy the thor:Context class
or one of its subtypes and click on the purple diamond button for
creating an instance of the selected class.

![](media/image38.png){width="5.645833333333333in"
height="6.010416666666667in"}

Once the instance is created, it only has an associated URI, being
without any other property. It is important to define a label for it -
without this step, the SKOS Generator is not going to work properly. To
do this, double click on the context instance name to go to its view and
click to add an "other property". Mark the show all button and select
rdfs:label.

## Metadata

All the thesaurus' elements receive meta-attributes to indicate
information about it or its creation. ThOR Data Model defines the
following metadata, which is implemented through Dublin Core ontology
terms.

![](media/image39.png){width="5.875in" height="4.197916666666667in"}

In Vocbench, all resources can receive these attributes. To do that,
open the desired resource view and click to add a new property (blue
square to add "other property", as indicated in the image below).

![](media/image40.png){width="6.267716535433071in"
height="3.9722222222222223in"}

At the new pop-up, click on "Show all", select the desired property to
be added and click on OK.

![](media/image41.png){width="6.267716535433071in"
height="4.736111111111111in"}

As can be seen in the ThOR Data Model, some metaproperties are related
to data values (literals) and others are related to resources. When an
attribute is going to be added, a pop-up is going to ask which is the
desired kind of range. Choose between resource (an entity with a URI) or
literal (a data value).

![](media/image42.png){width="6.267716535433071in"
height="1.9722222222222223in"}

When selected literal, choose a data type between one of the options
available (see image below), enter its data value, and click on OK. The
property is going to be created.

![](media/image43.png){width="6.267716535433071in"
height="3.9722222222222223in"}

When the resource option is selected, the resource to be related to the
thesaurus' element can be chosen between an already existing one or a
new resource can be created. At the left side of the pop-up, choose the
class which has the resource, or in which is going to be created the new
resource. At the right part, select the resource or create a new one
using the purple diamond icon at the top part of the view.

![](media/image44.png){width="6.267716535433071in"
height="3.1666666666666665in"}

A special case of resource is an Agent. Vocbench automatically opens a
specific pop-up for this case, when dcterms:contributor,
dcterms:creator, or dcterms:publisher is selected. In this case, select
or create a new Agent as desired.

![](media/image45.png){width="6.267716535433071in"
height="3.138888888888889in"}

# Exporting Data

For using ThOR's Ontolex data in the SKOS Generator it is necessary to
first export it from Vocbench. To do that, click on Global Data
Management (top right corner of the screen), and select the option
Export data.

At the new interface (seen in the figure below),
[[http://purl.org/ZIN-ThOR/]{.underline}](http://purl.org/ZIN-Thor/) is
already selected to be exported. Do not select any other graph option.
On Export Format (bottom right corner of the screen), select the option
Turtle. Check the box "Include inferred" and click the Submit button.

![](media/image46.png){width="6.267716535433071in"
height="3.3055555555555554in"}

After clicking on the Submit button, a pop-up is going to be shown.
Click on download for the generated file with the data.

[^1]: https://www.w3.org/TR/skos-reference/

[^2]: https://www.w3.org/2016/05/ontolex/

[^3]: https://github.com/ontolex/lexinfo

[^4]: https://www.w3.org/2016/05/ontolex/#lexicon-and-lexicon-metadata

[^5]: https://www.w3.org/2016/05/ontolex/

[^6]: https://www.w3.org/2016/05/ontolex/#canonicalForm

[^7]: https://www.w3.org/2016/05/ontolex/#lexical-sense-reference
